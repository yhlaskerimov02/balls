﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChukurSpawner : MonoBehaviour
{
    [SerializeField] GameObject chukur;

    private float startDelay = 5.5f;
    private float spawnInterval = 6f;
    void Start()
    {
        InvokeRepeating("ChukurSpawn", startDelay, spawnInterval);
    }

    private void LateUpdate()
    {
    }

    void ChukurSpawn()
    {
        Instantiate(chukur, new Vector3((int)(Random.Range(0, 4)) * 1.25f, 0.5f, 15), chukur.transform.rotation);
    }
}
