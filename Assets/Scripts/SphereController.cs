﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    [SerializeField] ParticleSystem particle;
    public static SphereController instance;
    public float speed = 5;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.Translate(Vector3.back * Time.deltaTime * speed);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("UnderGround"))
        {
            Destroy(gameObject);
        }


        if (gameObject.tag == "ConnectedBall" && other.gameObject.tag == "Chukur")
        {
            Destroy(gameObject);
            GameManager.instance.SetScore();
            MakeMagnetField();
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if ((gameObject.tag == "ConnectedBall") && collision.gameObject.tag != GameObject.Find("Player").gameObject.tag && collision.gameObject.tag != "Ground")
        {
            Debug.Log(collision.gameObject.transform.name);
            Destroy(gameObject);
            ParticleSystem newexplosion = Instantiate(particle, transform.position, particle.transform.rotation);
            Destroy(newexplosion, 2);
            MakeMagnetField();
        }
        
    }

    void MakeMagnetField()
    {
        MagneticField magneticField = GameObject.FindObjectOfType<MagneticField>();
        if (magneticField.boxCollider.center.z - 0.5f >= 0)
        {
            magneticField.ballNum -= 1;
            magneticField.boxCollider.center -= new Vector3(0, 0, 0.5f);
            magneticField.boxCollider.size -= new Vector3(0, 0, 1f);
            magneticField.offset -= new Vector3(0, 0, 1f);
        }
        if (magneticField.boxCollider.size.z == 1)
            GameObject.Find("Player").gameObject.tag = "Null";
    }
}
