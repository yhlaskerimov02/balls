﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public TextMeshProUGUI scoreText;
    public int score;
    private void Awake()
    {
        if (instance == null)
            instance = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Start()
    {
        scoreText = GameObject.Find("Score").GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetScore()
    {
        score++;
        scoreText.text = "" + score;
    }
}
