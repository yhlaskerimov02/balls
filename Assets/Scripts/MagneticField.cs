﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticField : MonoBehaviour
{
    PlayerController playerPos;
    public BoxCollider boxCollider;
    public Vector3 offset = new Vector3(0, 0, 1.1f);
    List <GameObject> spheres = new List<GameObject>();
    public float ballNum = 1;

    void Start()
    {
        playerPos = GameObject.Find("Player").GetComponent<PlayerController>();
        GameObject player = GameObject.Find("Player");
        boxCollider = player.GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter (Collider other)
    {
        if(gameObject.tag == "Null" && other.gameObject.tag != "Ground" && other.gameObject.tag != "Chukur")
        {
            gameObject.tag = other.gameObject.tag;
            other.transform.position = playerPos.transform.position + offset;
            boxCollider.center += new Vector3(0, 0, 0.5f);
            boxCollider.size += new Vector3(0, 0, 1f);
            other.gameObject.GetComponent<SphereController>().speed = 0;
            other.gameObject.AddComponent<FixedJoint>();
            other.gameObject.GetComponent<FixedJoint>().connectedBody = gameObject.GetComponent<Rigidbody>();
            offset += new Vector3(0, 0, 1f);
            other.gameObject.tag = "ConnectedBall";
            other.transform.name = "" + ballNum;
            ballNum ++;
        }
        else if(other.gameObject.tag  == gameObject.tag)
        {
            other.gameObject.tag = "ConnectedBall";
            other.transform.position = playerPos.transform.position + offset;
            boxCollider.center += new Vector3(0, 0, 0.5f);
            boxCollider.size += new Vector3(0, 0, 1f);
            other.GetComponent<SphereController>().speed = 0;
            other.gameObject.AddComponent<FixedJoint>();
            other.gameObject.GetComponent<FixedJoint>().connectedBody = gameObject.GetComponent<Rigidbody>();           
            offset += new Vector3(0, 0, 1f);
            other.transform.name = "" + ballNum;
            ballNum++;
        }
    }
}