﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawner : MonoBehaviour
{
    private float startDelay = 2;
    private float spawnInterval = 1f;
    List <int> indexes = new List<int>();
    List <int> initialIndexes = new List<int>();

    public List<GameObject> spheres;

    void Start()
    {

        for (int i = 0; i < 5; i++) {
            initialIndexes.Add(i);
            indexes.Add(i);
        }
        InvokeRepeating("SpawnRandomSphere", startDelay, spawnInterval);
    }


    void SpawnRandomSphere()
    {
        int rand = Random.Range(1, 5);
        for (int i = 1; i <= rand; i++)
        {
            int sphereIndex = Random.Range(0, spheres.Count);
            Vector3 spawnPos = RandomSpawnPosition();
            Instantiate(spheres[sphereIndex], spawnPos, spheres[sphereIndex].transform.rotation);
        }
        indexes.Clear();
        for (int i = 0; i < initialIndexes.Count; i++)
            indexes.Add(initialIndexes[i]);
    }

    Vector3 RandomSpawnPosition()
    {
        int spawnPosX = Random.Range(0, indexes.Count);
        Vector3 spawnPosition = new Vector3(indexes[spawnPosX]*1.25f, 0.5f, 15);
        indexes.RemoveAt(spawnPosX);
        return spawnPosition;
    }
}
